#coding:utf8


# LISTAS

lista2 = list("abracadabra")
lista = ['A', 'B', 'C']
print ('lista:', lista)

lista3.append(1) # a lista ficará assim [1]
lista3.append(3) # a lista ficará assim [1, 3] 

# insert(i, o), insere o objeto o na posição i (a primeira posição é zero)
lista3.insert(1, 2) # a lista ficará assim [1, 2, 3]
lista3.insert(0, 5) # a lista ficará assim [5, 1, 2, 3]

# remove(o) remove a primeira ocorrência do objeto o
lista3.append(5) # a lista ficará assim [5, 1, 2, 3, 5]
lista3.remove(5) # a lista ficará assim [1, 2, 3, 5]

# pop(i) remove e retorna o objeto da posição i
# pop() remove e retorna o último elemento da lista
lista3.insert(3, 6) # a lista ficará assim [1, 2, 3, 6, 5]
lista3.pop(3) # a lista ficará assim [1, 2, 3, 5] e retornará 6
lista3.pop() # a lista ficará assim [1, 2, 3] e retornará 5
 

#index(o) retorna o índice do objeto o
lista3.index(3) # retorna 2
lista3.append(3) # a lista ficará assim [1, 2, 3, 3]
lista3.index(3) # continua retornando 2 pois é a primeira ocorrência do 3
 

# count(o) conta quantas vezes o objeto o aparece na lista
lista3.count(1) # retorna 1
lista3.count(3) # retorna 2
lista3.pop() # a lista ficará assim [1, 2, 3] e retorna 3
 

# extend(L) estende a lista com a lista L
lista3.extend([0, -1, -2]) # a lista ficará assim [1, 2, 3, 0, -1, -2]
 

# sort() organiza os itens da lista nela mesma
lista3.sort() # a lista ficará assim [-2, -1, 0, 1, 2, 3]
 

# reverse() inverte os elementos da lista nela mesma
lista3.reverse() # a lista ficará assim [3, 2, 1, 0, -1, -2]



# A lista vazia é avaliada como falsa
while lista:
# Em filas, o primeiro item é o primeiro a sair
# pop(0) remove e retorna o primeiro item
    print ('Saiu', lista.pop(0), ', faltam', len(lista))
# Mais itens na lista
lista += ['D', 'E', 'F']
print ('lista:', lista)
while lista:
# Em pilhas, o primeiro item é o último a sair
# pop() remove e retorna o último item
    print ('Saiu', lista.pop(), ', faltam', len(lista))


O Inteiro representa os números inteiros, o ponto flutuante é usado para representar números decimais, e a cadeia de caracteres para representação de texto.

Porém, em Python, diferente de outras linguagens, estes tipos de dados, assim como todas as outras estruturas da linguagem são objetos, desta forma, o tipo básico de tudo em python é object. Há muito a se discutir sobre tipos de dados, mas para efeito de simplificação consideraremos assim.

Desta forma, cada classe que se cria em Python é um novo objeto, e assim também um novo tipo.

Voltando às estruturas de dados nativas, veremos que estas são conjuntos estruturados de objetos que podem conter elementos de qualquer outro tipo.

A primeira e talvez mais largamente usada destas estruturas é a list. Uma list implementa uma lista que pode conter virtualmente qualquer objeto, sendo composta por quantos tipos de objeto for necessário (diferente de algumas linguagens).

Desta forma,  listas se tornam uma ferramenta muito poderosa dentro do Python. Teremos mais adiante um post focado especialmente nas listas e seus métodos.

Para criar uma lista, basta atribuir valores a ela da seguinte forma:

l = [1,2,3,4,5]


Assim como fizemos quando vimos como funciona o for.

Outra estrutura de dados muito importante e largamente usada é a tuple (tupla). 
Tuplas contém conjuntos de valores assim como as listas, porém se diferenciam 
destas principalmente pelo fato de as tuplas não poderem ser alteradas após sua criação,
 e as listas sim.

Vamos criar uma tupla:

t = (1,2,3,4,5)


As tupals são as estruturas que a linguagem usa para passar parâmetros para funções .

Por exemplo, a função range, aceita valor inicial, valor final e passo.
 Estão, ao chamar no shell:

>>> range(1, 6, 2)
[1, 3, 5]

Os valores (1, 6, 2) são enviados para a função como uma tupla.

Vejamos agora uma das estruturas de dados mais poderosas do Python,
 e que possui muitas possibilidades de utilização. 
 Esta estrutura é o dict (dicionário) que relaciona pares de chave e valor.

Assim criamos um dicionário:

d = {'a':1, 'b':2,'c':3, 'd':4, 'e':5}


Finalmente, a última estrutura de dados nativa do Python que veremos hoje é o set
 (conjunto). Os sets tem um uso muito especial. Este comando cria listas a partir
 de uma lista, onde não há elementos repetidos.

Por exemplo, ao criar este set:

s = set([1, 1, 2, 3, 5, 7, 13, 17])


Neste caso, o conjunto será [1, 2, 3, 5, 7, 13, 17], pois 1 aparecia 2 vezes na lista original,
 e no set ele aparece somente uma vez.

Isto é muito útil, por exemplo, para saber, numa lista de números muito grande,
 quais são os números que aparecem, desconsiderando as repetições.
