import unittest
from fizzbuzz import *

class FizzBuzzTest(unittest.TestCase):

	def testTresRetornaFizz(self):
		self.assertEqual("Fizz",fizzBuzz(3))

	def testCincoRetornaBuzz(self):
		self.assertEqual("Buzz",fizzBuzz(5))

#	def testUmRetornaUm(self):
#		self.assertEqual("1",fizzBuzz(1))

	def testOnzeRetornaBatata(self):
		self.assertEqual("Batata",fizzBuzz(11))		

	def testQuinzeRetornaFizzBuzz(self):
		self.assertEqual("FizzBuzz",fizzBuzz(15))		

	def testCemRetornaBuzz(self):
		self.assertEqual("Buzz", fizzBuzz(100))

unittest.main()
