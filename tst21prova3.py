#coding:utf8
# Recebe um numero entre >=1 e <=20
# se fora do range, imprime o aviso
#----------------------------------
# se for par, imprime (é par);
# se for impar, imprime (é impar);
#----------------------------------
# imprime DEZENA se tiver dois digitos
# imprime UNIDADE se tiver apenas um
#----------------------------------
# calcula a raiz se for par e dezena
# calcula o quadrado se for impar e unidade
# para os outros, apenas mostra o numero.
import math
y=""

def paridade(x):
	if int(x)%2==0:
		print('numero eh PAR')
	else:
		print('numero eh IMPAR')

def dez(x):
	if len(x)==2:
		print('numero eh DEZENA')
	else:
		print('numero eh UNIDADE')

def calcula(x):
	if int(x)%2==0 and len(x)==2:
		print('numero eh PAR e DEZENA. Raiz calculada:',math.sqrt(int(x)))
	if int(x)%2!=0 and len(x)==1:
		print('numero eh IMPAR e UNIDADE. Quadrado calculado:',int(x)*int(x))
    
if __name__=='__main__':
    while y=="":
        y=input("Entre com x (entre >=1 e <=20): ")
        if y!="":
            if int(y)<1 or int(y)>20:
                print('numero esta fora da faixa permitida')
                y=""
    paridade(y)
    dez(y)
    calcula(y)
