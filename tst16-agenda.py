#coding:utf8
# Extenso de um numero digitado

import os, time

opcoes=('i','a','e','c','l','x')
menu=''
agenda={}

def insere(a,b):
    agenda[a]=b
    return print('Inserido')

def altera(a,b):
    agenda[a]=b
    return print('Alterado')

def exclui(a):
    agenda.pop(a)
    return print('Excluido')

def consulta(a):
    if a in agenda:
        n=a
        f=agenda[a]
    else:
        n='não encontrado'
        f='não encontrado'
    return print(n,f)

def lista():
	return print(agenda)


while menu!='x':
	menu=''
	print('#### MENU')
	print('Inserir')
	print('Alterar')
	print('Excluir')
	print('Consultar')
	print('listar')
	print('#########')

	while menu=="":
		menu=input("Qual a sua acao? ")
		if menu in opcoes:
			# i-inserir
			if menu==opcoes[0]:  
				nome=input('Nome:')
				fone=input('Fone:')
				insere(nome,fone)
				time.sleep(3)
			# a-alterar
			if menu==opcoes[1]: 
				nome=input('Nome:')
				consulta(nome)
				fone=input('Novo fone:')
				altera(nome,fone)
				time.sleep(3)
			# e-excluir
			if menu==opcoes[2]: 
				nome=input('Nome:')
				exclui(nome)
				time.sleep(3)
			# c-consultar
			if menu==opcoes[3]: 
				nome=input('Nome:')
				consulta(nome)
				time.sleep(3)
			# l-listar
			if menu==opcoes[4]: 
				lista()
				time.sleep(5)
				
	os.system('clear')
print('Obrigado por usar o programa! Até a próxima.')
