# coding:utf8
# Paises que continuam na copa 2014

import os, time

partidas={}

def inclui(a,b):
    partidas[a]=b
    return print('INCLUIDO!')

def altera(a,b):
    partidas[a]=b
    return print('ALTERADO!')

def exclui(a):
    partidas.pop(a)
    return print('EXCLUIDO')

def consulta(a):
    if a in partidas:
        n=a
        f=partidas[a]
    else:
        n='nao encontrado'
        f='nao encontrado'
    return print(n,f)

def lista():
    return print(partidas)

menu=''

while menu!='s':
    menu=""
    print('|---MENU---|')
    print('| Inserir  |')
    print('| Alterar  |')
    print('| Deletar  |')
    print('| Consultar|')
    print('| Listar   |')
    print('| Sair     |')
    print('|----------|')

    while menu=="":
        menu=input('Qual a acao desejada?')
        if menu!='':
            print('Menu: ',menu)

    if menu=='i':
        nome=input('Nome: ')
        fone=input('Fone: ')
        inclui(nome,fone)
        time.sleep(2)

    if menu=='a':
        nome=input('Nome: ')
        fone=input('Fone: ')
        altera(nome,fone)
        time.sleep(2)
	
    if menu=='d':
        nome=input('Nome: ')
        exclui(nome)
        time.sleep(2)

    if menu=='c':
        nome=input('Nome: ')
        consulta(nome)
        time.sleep(2)

    if menu=='l':
        lista()
        time.sleep(5)

    os.system('clear') #WINDOWS: cls       LINUX: clear
