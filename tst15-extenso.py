#coding:utf8
# Extenso de um numero digitado

x=""

def unidade(x1):
    num=x1[-1]
    if num=='0':
        d=x1[-2]
        if d==' ':
            resun="zero"
        else:
            resun=""
    if num=='1':
        resun="um"
    if num=='2':
        resun="dois"
    if num=='3':
        resun="tres"
    if num=='4':
        resun="quatro"
    if num=='5':
        resun="cinco"
    if num=='6':
        resun="seis"
    if num=='7':
        resun="sete"
    if num=='8':
        resun="oito"
    if num=='9':
        resun="nove"
    return (resun)

def dezena(y):
    num=y[-2]
    if num=='0':
        resd=''
    if num=='1':
        uni=y[-1]
        if uni=='0':
            resd='dez'
        if uni=='1':
            resd='onze'
        if uni=='2':
            resd="doze"
        if uni=='3':
            resd="treze"
        if uni=='4':
            resd="quatorze"
        if uni=='5':
            resd="quinze"
        if uni=='6':
            resd="dezesseis"
        if uni=='7':
            resd="dezessete"
        if uni=='8':
            resd="dezoito"
        if uni=='9':
            resd="dezenove"
    if num=='2':
        resd='vinte'
    if num=='3':
        resd="trinta"
    if num=='4':
        resd="quarenta"
    if num=='5':
        resd="cinquenta"
    if num=='6':
        resd="sessenta"
    if num=='7':
        resd="setenta"
    if num=='8':
        resd="oitenta"
    if num=='9':
        resd="noventa"
    return (resd)

def centena(y):
    num=y[-3]
    if num=='0':
        resct=''
    if num=='1':
        dz1=y[-2]
        un1=y[-1]
        if dz1=='0' and un1=="0":
            resct='cem'
        else:
            resct='cento'
    if num=='2':
        resct='duzentos'
    if num=='3':
        resct="trezentos"
    if num=='4':
        resct="quatrocentos"
    if num=='5':
        resct="quinhentos"
    if num=='6':
        resct="seiscentos"
    if num=='7':
        resct="setecentos"
    if num=='8':
        resct="oitocentos"
    if num=='9':
        resct="novecentos"
    return (resct)


while x=="":
    x=input("Entre com x: ")
    if x!="":
        if int(x)<0 or int(x)>=1000:
            x=""

tamanho=len(x)

if tamanho==1:
    x=' '+x
    resultado=unidade(x)

if tamanho==2:
    if x[-2]=='1':
        resultado=dezena(x)
    else:
        if x[-1]=='0':
            liga=''
        else:
            liga=' e '
        resultado=dezena(x)+liga+unidade(x)
        #          vinte      e    dois

if tamanho==3:
    if x[-3]=='1' and x[-2]=='0' and x[-1]=="0":
        resultado=centena(x)

    if x[-3]=="0":
        if x[-2]=='1':
            resultado=dezena(x)
        else:
            if x[-1]=='0':
                liga=''
            else:
                liga=' e '
            resultado=dezena(x)+liga+unidade(x)
    else:
        liga1=' e '
        liga=' e '
        if x[-2]=='0':
            liga=''
        if x[-1]=='0':
            liga1=''
        resultado=centena(x)+liga+dezena(x)+liga1+unidade(x)
print('O numero '+x+' pode ser escrito como '+resultado)

