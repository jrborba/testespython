#coding:utf8
# Extenso de um numero digitado

import os, time
agenda={}

arquivo=open('dados.dat','r+')
agenda=arquivo.read()
arquivo.close()

opcoes=('i','a','e','c','l','x')
menu=''

def insere(a,b):
    agenda[a]=b
    arquivo=open('dados.dat','w')
    arquivo.write(agenda)
    arquivo.close()
    return print('Inserido')

def altera(a,b):
    agenda[a]=b
    arquivo=open('dados.dat','w')
    arquivo.write(agenda)
    arquivo.close()
    return print('Alterado')

def exclui(a):
    agenda.pop(a)
    arquivo=open('dados.dat','w')
    arquivo.write(agenda)
    arquivo.close()
    return print('Excluido')

def consulta(a):
    if a in agenda:
        n=a
        f=agenda[a]
    else:
        n='não encontrado'
        f='não encontrado'
    return print(n,f)

def lista():
	return print(agenda)


while menu!='x':
	menu=''
	print('#### MENU')
	print('Inserir')
	print('Alterar')
	print('Excluir')
	print('Consultar')
	print('listar')
	print('#########')

	while menu=="":
		menu=input("Qual a sua acao? ")
		if menu in opcoes:
			# i-inserir
			if menu==opcoes[0]:  
				nome=input('Nome:')
				fone=input('Fone:')
				insere(nome,fone)
				input('Pressione enter para continuar...')
			# a-alterar
			if menu==opcoes[1]: 
				nome=input('Nome:')
				consulta(nome)
				fone=input('Novo fone:')
				altera(nome,fone)
				input('Pressione enter para continuar...')
			# e-excluir
			if menu==opcoes[2]: 
				nome=input('Nome:')
				exclui(nome)
				input('Pressione enter para continuar...')
			# c-consultar
			if menu==opcoes[3]: 
				nome=input('Nome:')
				consulta(nome)
				input('Pressione enter para continuar...')
			# l-listar
			if menu==opcoes[4]: 
				lista()
				input('Pressione enter para continuar...')
				
	os.system('clear')
print('Obrigado por usar o programa! Até a próxima.')

"""
mydict = {
    'NATION': {'sales_max': 1000, 'sales_min': 500, ...}, 
    'LOCAL':  {'sales_max': 250, 'sales_min': 0, ...},
    'REGION': {'sales_max': 500, 'sales_min': 250, ...}, 
    ...
}
mydict[k1][k2] 
