#coding:utf8
# criar lista
# inserir 10 numeros pelo teclado
# numeros entre >0 e <=20
#----------------------------------
# mostrar a lista na forma que entrou
# ordena a lista em ordem crescente e mostra
# mostra o numero de elementos na lista
# eliminar o quinto elemento da lista
# mostrar a lista como ficou

y=""
x=0

lista=list()

if __name__=='__main__':
    while y=="" or x<10:
        y=input("Entre com x (entre >=1 e <=20): ")
        if y!="":
            if int(y)<1 or int(y)>20:
                print('numero esta fora da faixa permitida')
                y=""
            else:
                lista.append(y)
        x=x+1

print (lista)
lista.sort()
print (lista)
print(len(lista)," elementos na lista")
lista.pop(5)
print (lista)
print(len(lista)," elementos na lista")
