import collections

def unique_chars(mystring):
    """ Iterate over strings to determine if only
        have unique chars. If the chars are unique,
        return True. Else, return False.  """
    unique=True
    mc=collections.Counter(mystring).most_common(1)
    if mc[0][1]>1:
        unique=False
    return unique
		
#d='Mari'
#print(unique_chars(d))
