import collections

class unique_chars:
    """ Iterate over strings to determine if only
        have unique chars. If the chars are unique,
        return True. Else, return False.  """

    def __init__(self, mystring):
        self.mystring=mystring
        self.unique=True
		
    def verify_unique_chars(self):
        mc=collections.Counter(self.mystring).most_common(1)
        if mc[0][1]>1:
            self.unique=False
        return self.unique
		
d='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1233'
y=unique_chars(d)
print(y)
print (y.verify_unique_chars())
