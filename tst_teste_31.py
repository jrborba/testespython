import unittest
from tst_31 import unique_chars

alfa='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
num='1234567890'
alfap1='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZa'
nump1='12345678901'

class Unique_Chars_Test(unittest.TestCase):

	def testAlfareturnTrue(self):
		self.assertTrue(unique_chars(alfa))

	def testNumreturnTrue(self):
		self.assertTrue(unique_chars(num))

	def testAlfaplus1returnFalse(self):
		self.assertFalse(unique_chars(alfa))

	def testNumplus1returnFalse(self):
		self.assertFalse(unique_chars(nump1))

unittest.main()
