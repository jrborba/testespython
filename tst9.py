#coding: utf-8
# Algoritmo para testar se os números de um intervalo são múltiplos por 3, por 5 E,
# simultaneamente por 3 e por 5.
     
valor=""
parecer= ""
min= 0
     
     
print ('************ Programa de teste de divisão ****************')
     
     
while valor=="":
# pedir que o usuário digite um valor:
    valor=input("Insira um valor: ")
# testa se o valor é diferente de nulo(if) e converte para int, caso contrário(else), limpa
# a variável e repete o pedido de valor
    if valor!="":
        min=int(valor)      
    #valor=float(valor)      
    else:
            valor=""
     
# inicia o laço for com os testes por 3, por 5 e por 3 E 5:
for x in range(min, 1001):
# testando com 3
    if x%3.==0 and x%5.!=0:
        print (x, " é múltiplo de 3")
# testando com 5
    if x%3.!=0 and x%5.==0:
        print (x, " é múltiplo de 5")  
# testando com 3 e 5      
    if x%3.==0  and x%5.==0:
       print (x, " é múltiplo de 3 e 5")
